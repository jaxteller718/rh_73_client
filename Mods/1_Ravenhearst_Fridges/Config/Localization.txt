﻿Key,Source,Context,English
cntCoolerClosedRH,blocks,Container,Cooler (Storage)
cntRetroFridgeVer1ClosedRH,blocks,Container,Retro Refrigerator (Storage)
cntFridgeStainlessSteelRH,blocks,Container,Steel Refrigerator (Storage)
cntIceMachineRH,blocks,Container,Rotting Ice Chest (Storage)
cntCoolerClosedRHDesc,blocks,Container,A nice way of storing your food and crops.
cntRetroFridgeVer1ClosedRHDesc,blocks,Container,A nice way of storing your food and crops.
cntIceMachineRHDesc,blocks,Container,A nice way of storing your food and crops.
preservationBarrelRH,blocks,Block,Food Barrel (Storage),,,,,
preservationBarrelRHDesc,blocks,Block,This barrel is a nice way of storing your food and crops.,,,,,