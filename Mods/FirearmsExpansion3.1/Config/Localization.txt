﻿Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

Chaingun,items,Gun,New,M134 Minigun,,,,,
ChaingunDesc,items,Gun,New,This thing that uses 7.62mm Ammo doesn't need a description.\n\nNot craftable Can be found only in military munitions ammo box or bought from traders.\nRepair with a Gun Repair Kit.\nScrap to Automatic Rifle Pieces.,,,,,

Flamethrower,items,Gun,New,Flamethrower,,,,,
FlamethrowerDesc,items,Gun,New,This is an anti-horde weapon that uses Gas Can as ammo\nLocated under Advanced Engineering\n\nRepair with a Gun Repair Kit\nScrap to Motor Tool Parts.,,,,,

Railgun,items,Gun,New,Railgun,,,,,
RailgunDesc,items,Gun,New,Homemade Hi-tech weapon that uses Iron Dart as ammo\nLocated under Electrocutioner\nThis weapon can not be found in loot\n\nRepair with a Gun Repair Kit\nScrap to Stun Baton Parts.,,,,,

M98,items,Gun,New,M98 Sniper Rifle,,,,,
M98Desc,items,Gun,New,Very Powerful Bolt-action rifle that uses 7.62mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Rifle Pieces.,,,,,

Mac10,items,Gun,New,Mac 10,,,,,
Mac10Desc,items,Gun,New,Weak but fast Automatic Submachine Gun that uses 9mm Ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Handgun Pieces.,,,,,

Glock40,items,Gun,New,Glock 40,,,,,
Glock40Desc,items,Gun,New,SemiAutomatic Glock that uses 9mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Handgun Pieces.,,,,,

SCAR,items,Gun,New,SCAR,,,,,
SCARDesc,items,Gun,New,Automatic Assault Rifle that uses 7.62mm Ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Automatic Rifle Pieces.,,,,,

KSG12,items,Gun,New,KSG12,,,,,
KSG12Desc,items,Gun,New,Powerful SemiAutomatic Shotgun that uses all Shell Types.\n\nRepair with a Gun Repair Kit.\nScrap to Shotgun Pieces.,,,,,

JNG90,items,Gun,New,JNG90 Sniper Rifle,,,,,
JNG90Desc,items,Gun,New,Powerful Bolt-action rifle that uses 7.62mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Rifle Pieces.,,,,,

Thompson,items,Gun,New,Thompson,,,,,
ThompsonDesc,items,Gun,New,Old and legendary Automatic Submachine Gun that uses 9mm Ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Handgun Pieces.,,,,,

Colt38,items,Gun,New,Colt 38 Special,,,,,
Colt38Desc,items,Gun,New,Weak but reliable small pistol that uses 9mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Handgun Pieces.,,,,,

Colt1911,items,Gun,New,Colt 1911,,,,,
Colt1911Desc,items,Gun,New,Powerful iconic pistol that uses 9mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Handgun Pieces.,,,,,

FNFAL,items,Gun,New,FNFAL,,,,,
FNFALDesc,items,Gun,New,SemiAutomatic Assault Rifle that uses 7.62mm Ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Automatic Rifle Pieces.,,,,,

Winchester,items,Gun,New,Winchester,,,,,
WinchesterDesc,items,Gun,New,Powerful PumpShotgun that uses all Shell Types.\n\nRepair with a Gun Repair Kit.\nScrap to Shotgun Pieces.,,,,,

M1garand,items,Gun,New,M1 garand,,,,,
M1Desc,items,Gun,New,Old and legendary SemiAutomatic rifle that uses 7.62mm ammo.\n\nRepair with a Gun Repair Kit.\nScrap to Rifle Pieces.,,,,,

gunSpecialHandgunsSchematic,items,Gun,New,Elite Handguns Schematic,,,,,
gunSpecialShotgunsSchematic,items,Gun,New,Elite Shotguns Schematic,,,,,
gunSpecialAutosSchematic,items,Gun,New,Elite Automatics Schematic,,,,,
gunSpecialRiflesSchematic,items,Gun,New,Elite Rifles Schematic,,,,,

