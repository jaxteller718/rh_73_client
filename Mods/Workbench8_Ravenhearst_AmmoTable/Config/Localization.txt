﻿Key,Source,Context,English
ammoTableRH,blocks,Workstation,Ammo Table,,,,,
ammoTableRHDesc,blocks,Workstation,"The ammo table is more efficient at crafting ammunition at greater yields.",,,,,
ammoTableRHSchematic,blocks,Workstation,Ammo Table Schematic,,,,,
toolCalipersRH,items,Item,Calipers,,,,,
toolCalipersRHDesc,items,Item,Calipers are essential for disassembling ammo.,,,,,
