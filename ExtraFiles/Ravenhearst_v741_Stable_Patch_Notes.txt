===============================================

Major: Added Sphere Code Patch for Servers. This should reduce idle server usage from 12g to 7-8g. More work will be done in 7.5. IMPORTANT NOTE: This is a server side only optimization. It works by forcing the game to load textures as you see them and not preload every texture in the mod. You may see some hitches and stutters when things load in the first time. If this is an issue for you and if you have your own server that can normally handle RH then feel free to delete the ZZZ-SphereServerLagFix folder found in the server Data/Configs folder. If you do not have access to the server then this is the price we pay to have under 12g of memory used in this game for a mod so large.
Change: Pallets now drop between 1 and 2 cobblestone or cement
Change: Wood Logs now stack to 250
Change: Recipe Adjustments for Iron Gates and Doors


Fix: Yellow warnings on load from Terrain Movement
Fix: Armored Hazmat should now take mods
Fix: Iron Gates can now be repaired
Fix: Incorrect lock info for Legendary Bow
Fix: Discrepencies in journal and tips for Irrigation
Fix: Horses were not giving proper meats and bones

POI: Large Book Store - Fixed missing sleepers, removed t5 rad damage, multiple fixes and changes to interior


Updated: Z2






